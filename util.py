import networkx as nx
import sys
sys.setrecursionlimit(10000)
import random

inc = ({x+1: {random.randint(1, 30) for y in range(x, x + random.randint(1, 4))} for x in range(30)})
# print(inc)
# # (('A', 'B', 'C'), [('A', 'B'), ('B', 'C')])

def swap():
    return None

def graph_loader(graph_file):
    with open(graph_file, 'r') as gfile:
        nodes = tuple(gfile.readline().rstrip())
        edges = [tuple(e.rstrip()) for e in gfile]
    return nodes, edges


def get_nx_graph(graph_data):
    graph = nx.Graph()

    for node in graph_data[0]:
        graph.add_node(node)

    for u, v in graph_data[1]:
        graph.add_edge(u, v)

    return graph


def get_my_graph(graph_data):
    g = dict()

    return g


# def my_shortest_path(my_graph, param, param1):
#     return None  # Посещена ли вершина?
# def dfs(v):
#     visited = set()
#     if v in visited:  # Если вершина уже посещена, выходим
#         return
#     visited.add(v)  # Посетили вершину v
#     for i in v[inc]:  # Все смежные с v вершины
#         if not i in visited:
#             dfs(i)


def dfs(v):
    visited = set()
    if v in visited:  # Если вершина уже посещена, выходим
        return
    visited.add(v)  # Посетили вершину v
    for i in inc[v]:  # Все смежные с v вершины
        if not i in visited:
            dfs(i)


# print(dfs(1))


# Поиск в глубину - ПВГ (Depth First Search - DFS)
def all_paths(graph, start, goal):
    queue = [(start, [start])]
    while queue:
        (v, path) = queue.pop(0)
        for next in graph[v] - set(path):
            if next == goal:
                yield path + [next]
            else:
                queue.append((next, path + [next]))
            yield queue
    min_path = min(all_paths(graph, start, goal), key=len)
# print(min(all_paths(inc, 1, 100),key=len))

def bfs_paths(graph, start, goal):
    queue = [(start, [start])]
    while queue:
        swap()
        (vertex, path) = queue.pop(0)
        for next in graph[vertex] - set(path):
            swap()
            if next == goal:
                yield path + [next]
            else:
                swap()
                queue.append((next, path + [next]))

print(list(bfs_paths(inc, 1, 30))) # [['A', 'C', 'F'], ['A', 'B', 'E', 'F']]

def shortest_path(graph, start, goal):
    try:
        swap()
        return next(bfs_paths(graph, start, goal))
    except StopIteration:
        return None

print(shortest_path(inc, 1, 30)) # ['A', 'C', 'F']